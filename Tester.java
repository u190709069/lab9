import ThreeDimensionalShape.Cube;
import ThreeDimensionalShape.Sphere;
import ThreeDimensionalShape.Tetrahedron;
import TwoDimensionalShape.Circle;
import TwoDimensionalShape.Square;
import TwoDimensionalShape.Triangle;
import test.test.Shape;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Tester {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner input = new Scanner(new File("Instructor.txt"));
        Shape shape;
        ArrayList<Shape> shapes = new ArrayList<Shape>();

        Circle circle = new Circle(5);
        while (input.hasNextLine()) {
            String line = input.nextLine();
            String[] index = line.split(" ");
            if (index[0].equals("O") && index[1].equals("C")) {

                double radius = Double.parseDouble(index[2]);
                shape = new Circle(radius);
                shapes.add(shape);
                System.out.println(shape);

            } else if (index[0].equals("O") && index[1].equals("S")) {
                double SideLenght = Double.parseDouble(index[2]);
                shape = new Square(SideLenght);
                shapes.add(shape);
                System.out.println(shape);

            } else if (index[0].equals("O") && index[1].equals("T")) {
                double baseOfTriangle = Double.parseDouble(index[2]);
                double firstLengthOfTheTriangle = Double.parseDouble(index[3]);
                double secondLengthOfTheTriangle = Double.parseDouble(index[4]);
                double heightOfTheTriangle = Double.parseDouble(index[5]);
                shape = new Triangle(baseOfTriangle, firstLengthOfTheTriangle, secondLengthOfTheTriangle, heightOfTheTriangle);
                shapes.add(shape);
                System.out.println(shape);

            }
            else if (index[0].equals("O") && index[1].equals("SP")) {
                double sideOfTheSphere = Double.parseDouble(index[2]);
                shape = new Sphere(sideOfTheSphere);
                shapes.add(shape);
                System.out.println(shape);
            }

            else if (index[0].equals("O") && index[1].equals("CU")) {
                double SideLenght = Double.parseDouble(index[2]);
                shape = new Cube(SideLenght);
                shapes.add(shape);
                System.out.println(shape);
                
            }
            else if (index[0].equals("O") && index[1].equals("TE")) {
                double sideOfTheTetrahedron = Double.parseDouble(index[2]);
                shape = new Tetrahedron(sideOfTheTetrahedron);
                shapes.add(shape);
                System.out.println(shape);


            }
            else if (index[0].equals("TA")) {

                double sumOfTheAreas = 0;
                for (int i = 1; i < shapes.size(); i++) {

                    sumOfTheAreas += shapes.get(i).area();

                }
                System.out.println("total area: "+ sumOfTheAreas);


            }

            else if (index[0].equals("TP")) {

                double sumOfThePerimeters = 0;
                for (int i = 1; i < shapes.size(); i++) {

                    sumOfThePerimeters += shapes.get(i).perimeter();

                }
                System.out.println("total perimeter: "+sumOfThePerimeters);


            }

            else if (index[0].equals("TV")) {

                double sumOfTheVolumes = 0;
                for (int i = 1; i < shapes.size(); i++) {

                    sumOfTheVolumes += shapes.get(i).volume();

                }
                System.out.println("total volume: "+sumOfTheVolumes);


            }


        }


    }
}