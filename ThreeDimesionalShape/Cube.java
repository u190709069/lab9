package ThreeDimensionalShape;


import TwoDimensionalShape.Square;

public class Cube extends Square {

    public Cube(double SideLenght) {
        super(SideLenght);
    }


    public double area(){
        return super.area();
    }
    public double perimeter() {
        return (super.perimeter() * SideLenght);
    }

    public double Volume()
    {
        return (super.area());
    }

    @Override
    public String toString(){
        return "side: "+SideLenght;
    }


}