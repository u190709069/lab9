package ThreeDimensionalShape;

import TwoDimensionalShape.Circle;

public class Sphere extends Circle {

    public Sphere(double radius) {
        super(radius);

    }
    public double area(){
        return 4*Math.PI*radius*radius;
    }

    public double volume(){
        return 4*Math.PI*radius*radius*radius/3;
    }

    public double perimeter(){
        return 2*Math.PI*radius;
    }

    @Override
    public String toString() {
        return "Sphere{" +
                "radius=" + radius +
                '}';
    }
}