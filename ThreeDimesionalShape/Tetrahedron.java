package ThreeDimensionalShape;

import test.test.Shape;

public class Tetrahedron extends Shape {

    public double sideOfTheTetrahedron;

    public Tetrahedron(double side) {
        this.sideOfTheTetrahedron = side;
    }

    public double area(){
        return sideOfTheTetrahedron*sideOfTheTetrahedron*Math.sqrt(3);

    }

    public double volume(){
        return sideOfTheTetrahedron*sideOfTheTetrahedron*sideOfTheTetrahedron*Math.sqrt(2)/12;
    }

    public double perimeter(){
        return sideOfTheTetrahedron*6;
    }

    @Override
    public String toString() {
        return "Tetrahedron{" + "side=" + sideOfTheTetrahedron + '}';
    }
}