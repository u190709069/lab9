package TwoDimensionalShape;


import test.test.Shape;

public class Square extends Shape {

    public double SideLenght;

    public Square(double SideLenght) {
        this.SideLenght = SideLenght;


    }

    public double perimeter(){
        return SideLenght*4;

    }

    public double area(){
        return SideLenght*SideLenght;
    }

    @Override
    public String toString(){
        return "side: "+SideLenght;
    }

}