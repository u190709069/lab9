package TwoDimensionalShape;

import test.test.Shape;

public class Triangle extends Shape {


    public double baseOfTriangle;
    public double firstLengthOfTheTriangle;
    public double secondLengthOfTheTriangle;
    public double heightOfTriangle;

    public Triangle(double baseOfTriangle, double firstLengthOfTheTriangle, double secondLengthOfTheTriangle, double heightOfTriangle) {
        this.baseOfTriangle = baseOfTriangle;
        this.firstLengthOfTheTriangle = firstLengthOfTheTriangle;
        this.secondLengthOfTheTriangle = secondLengthOfTheTriangle;
        this.heightOfTriangle = heightOfTriangle;

    }

    public double perimeter() {
        return baseOfTriangle+firstLengthOfTheTriangle+secondLengthOfTheTriangle ;

    }
    public double area(){
        return (baseOfTriangle*heightOfTriangle)/2;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "lengthOfTheTaban=" + baseOfTriangle +
                ", lengthOfTheTriangle2=" + firstLengthOfTheTriangle +
                ", lengthOfTheTriangle3=" + secondLengthOfTheTriangle +
                ", heightOfTheTriangle=" + heightOfTriangle +
                '}';
    }
}